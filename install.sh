#!/bin/sh
REPO="arch-system-config-files"
GIT_DIR="$HOME/.$REPO"
WORK_TREE="/"
GIT_COMMAND="git --git-dir=$GIT_DIR --work-tree=$WORK_TREE"

# Only allow this script to be ran as root
if [ $EUID -ne 0 ]; then
	echo "Please run this script as root."
	exit
fi

# Clone the repo
git clone --bare https://gitlab.com/Thilawyn/$REPO.git "$GIT_DIR"

# Make sure untracked files don't show up (very annoying)
$GIT_COMMAND config --local status.showUntrackedFiles no

# Install the files
$GIT_COMMAND checkout
echo -e "\nDone. If (and only if) the last command failed, please remove the incriminating files and run:\n\t$GIT_COMMAND checkout"
echo -e "\nAlso, don't forget to set your name and email before committing:\n\tgit config --global user.name \"your_name\"\n\tgit config --global user.email \"your_email\""
